parolalar = [] #Parolalar listesi olusturuldu.
while True:
    print('Bir işlem seçin')  #Mesaj consola basıldı.
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :') #Secenekler icin kullanıcıdan girdi istendi.

    if islem.isdigit():
        islem_int = int(islem)

        if islem_int not in [1, 2]: #Kullanıcı girdisi belirtilen 1 ve 2 numaraları secenek değil ise koşulu
            print('Hatalı işlem girişi') #İf kosulu saglanıyorsa consola basar.
            continue #While döngüsünün basına döner ve döngü bastan baslar.

        if islem_int == 2: #Kullanıcı 2 secenegini secti ise kosulu uygulanır.

            #Kullanıcıdan girdi ismi, kullanici  adi, parola, e-pposta ve gizli soru girdileri alınır.
            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')
            kullanici_adi = input('Kullanici Adi Girin :')
            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')

            #Kullanıcıdan alınan kullanıcı adı, parola, eposta, gizli soru cevabı, girdi ismi ve parola2 girdilerinin kontrolü yapılır.
            #Kullanıcı herhangi bir şey girmez ise istenilen mesaj ekrana bastırılır ve doğru giriş yapılana kadar if basina continue ile dönülür.
            if kullanici_adi.strip() == '':
                print('kullanici_adi girmediz')
                continue
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue

            #Bir dict olusuturuldu.
            yeni_girdi = {
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi) #parolalar listesine yeni girdi dict'i  icindekiler eklenmis oldu.
            continue #İslemler yapıldıgı süre boyunca while döngüsünü tekrar tekrar döndürür.

        elif islem_int == 1: #Kullanıcı 1. secenegi secer ise
            alt_islem_parola_no = 0
            for parola in parolalar: #Parola degiskeni parolalar listesi boyunca dolasır.
                alt_islem_parola_no += 1 #Liste dolastıkca bu deger artar.

                # Kaydedilen parolalar, parolalar listesinden get ile cekilerek ekrana basılır.
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))
            alt_islem = input('Yukarıdakilerden hangisi ?: ') # Kaydedilen parolalar arasindan secim yapılması istenir.
            if alt_islem.isdigit(): # Girdi bir sayi mi? kontrolü yapılır.
                # Girdi bir sayi ise  alt_islem girdisi 1 den kücük ve parolalar listesinin boyutundan kücük olup olmadıgı kontrolü yapılır.
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem): #Kosul doğruysa ekrana hata mesajı basılır.
                    print('Hatalı parola seçimi')
                    continue #Dogru girdi alınana kadar if kosulu dönmeye devam eder.

                parola = parolalar[int(alt_islem) - 1]   #?

                #Parolalar listesinden bilgiler cekilerek ekrana basılır.
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue #İslemler yapıldıgı sürece if döngüsü devam eder.

    #While döngüsünün her yanlıs durumu icin ekrana hata mesajı basılır.
    print('Hatalı giriş yaptınız')
